# Microsoft SQL Server - Ubuntu Linux Container

## Preface

These files are for educational use only and should not be used within any production environment

## Files

### DockerFile

Docker file is pretty straight forward. Please read the comments within that file, but otherwise I am not going to spend much time here. 

#### entrypoint.sh

This script is used as an entry point for docker. On top of SQL Server, we also run our database initization script. 

#### run-initialization.sh

This script will kick off and run any contrainer initialization that needs ot be completed.

#### init.sql

Add any sql you would like to execute

### docker-compose.yml

Again this is pretty straight forward with nothing advance going on. Perhaps the only thing to note is we use docker compose to build our docker image. This allows us to have to only run one command to get everything up and running. 

```txt
docker compose up
```

Just make sure you adjust your volumes before hand. 

## Volumes

In addition to the standard SQL Volumes, there is an additional "dropbox" volume. This can be used for importing *.bak files in the case of a restore. 

## Related Links

- [Docker Hub - MSSql Server](https://hub.docker.com/_/microsoft-mssql-server)
